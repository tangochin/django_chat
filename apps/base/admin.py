from django.contrib import admin

from .models import Message


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('id', 'text', 'sender')
    raw_id_fields = ('sender', )

    def get_queryset(self, request):
        return super().get_queryset(request).select_related('sender')
