from django.contrib.auth.models import User
import factory
from factory import fuzzy

from .models import Message


class UserFactory(factory.django.DjangoModelFactory):
    username = factory.Sequence(lambda n: 'user%d' % n)
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    email = factory.Faker('email')
    password = '123'
    is_active = True

    class Meta:
        model = User


class MessageFactory(factory.DjangoModelFactory):
    sender = factory.SubFactory(UserFactory)
    text = fuzzy.FuzzyText(length=100)

    class Meta:
        model = Message
