from django.db import models
from django.conf import settings


class Message(models.Model):
    TEXT_MAX_LENGTH = 1000

    sender = models.ForeignKey(
        to=settings.AUTH_USER_MODEL,
        verbose_name='Отправитель',
        on_delete=models.CASCADE
    )
    text = models.CharField(
        verbose_name='Текст',
        max_length=TEXT_MAX_LENGTH
    )
    date_send = models.DateTimeField(
        verbose_name='Дата отправки',
        auto_now_add=True
    )

    class Meta:
        verbose_name = 'Сообщение'
        verbose_name_plural = 'Сообщения'

    def __str__(self):
        return 'Сообщение #%i: %s' % (self.id, self.text[:50])
