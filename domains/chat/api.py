from django.contrib.auth import authenticate, login
from rest_framework.decorators import api_view
from rest_framework.permissions import IsAuthenticated
from rest_framework import viewsets
from rest_framework.response import Response

from apps.base.models import Message
from .serializers import MessageSerializer, LoginSerializer
from .filters import MessageListFilter


class MessagesViewSet(viewsets.ModelViewSet):
    """
    API of messages.
    """
    http_method_names = ['get', 'post', 'head', 'options', 'trace']
    permission_classes = (IsAuthenticated,)
    serializer_class = MessageSerializer
    filter_class = MessageListFilter
    ordering = ('-date_send',)

    def get_queryset(self):
        return Message.objects.order_by('date_send')


@api_view(http_method_names=['POST'])
def login_api_view(request):
    auth_error = {'__form__': 'Incorrect username or password.'}
    if request.method == 'POST':
        serializer = LoginSerializer(data=request.data)
        if serializer.is_valid():
            user = authenticate(request, **serializer.data)
            if user is None:
                return Response(auth_error, status=403)
            login(request, user)
            return Response({}, status=200)
    return Response(auth_error, status=403)
