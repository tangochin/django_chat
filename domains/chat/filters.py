from django_filters import rest_framework as filters

from apps.base.models import Message


class MessageListFilter(filters.FilterSet):
    """
    Filter class for list of messages.
    """
    last_message = filters.NumberFilter(field_name='id', lookup_expr='gt')
    limit = filters.NumberFilter(method='filter_limit')

    class Meta:
        model = Message
        fields = ('id',)

    def filter_limit(self, queryset, name, value):
        if value:
            queryset = queryset.filter(
                id__in=queryset.order_by('-id')[:value]
            ).order_by('id')
        return queryset
