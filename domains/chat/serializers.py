import html

from rest_framework import serializers

from apps.base.models import Message


class MessageSerializer(serializers.ModelSerializer):
    """
    Chat message serializer.
    """
    sender = serializers.CharField(source='sender.get_full_name', read_only=True)

    class Meta:
        model = Message
        fields = ('id', 'text', 'sender', 'date_send')

    def create(self, validated_data):
        request = self.context.get('request')
        text = validated_data.get('text')
        text = html.escape(text).replace('\n', '<br>')
        text = text[:Message.TEXT_MAX_LENGTH]
        message = Message.objects.create(
            sender=request.user,
            text=text
        )
        return message


class LoginSerializer(serializers.Serializer):
    """
    Login API serializer.
    """
    username = serializers.CharField(max_length=150, required=True)
    password = serializers.CharField(max_length=128, required=True)
