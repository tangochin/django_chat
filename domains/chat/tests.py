from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APITestCase, APIRequestFactory

from apps.base.factories import UserFactory, MessageFactory
from apps.base.models import Message
from domains.chat.api import login_api_view


class LoginViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user('foobar', 'foo@bar.com', '123')
        cls.url = reverse('login')

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        self.client.login(username='foobar', password='123')
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)


class ChatViewTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user = User.objects.create_user('foobar', 'foo@bar.com', '123')
        cls.url = reverse('chat')

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 302)
        self.client.login(username='foobar', password='123')
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)


class MessagesViewSetTest(APITestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user1 = UserFactory.create(
            username='foobar',
            email='foo@bar.com',
            password='123',
            first_name='Bar',
            last_name='Foo'
        )
        cls.user2 = UserFactory.create(
            username='asd',
            email='asd@dsa.com',
            password='123',
            first_name='Asd',
            last_name='Dsa'
        )
        cls.msg1 = MessageFactory.create(sender=cls.user1)
        cls.msg2 = MessageFactory.create(sender=cls.user1)
        cls.msg3 = MessageFactory.create(sender=cls.user2)
        cls.url = reverse('api-messages-list')

    def test_get(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 403)
        self.client.force_authenticate(user=self.user1)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200)
        data = response.json()
        self.assertIn('count', data)
        self.assertIn('next', data)
        self.assertIn('previous', data)
        self.assertIn('results', data)
        self.assertEqual(data['count'], 3)
        messages = [msg for msg in data['results']]
        ids = [msg['id'] for msg in messages]
        self.assertIn(self.msg1.id, ids)
        self.assertIn(self.msg2.id, ids)
        self.assertIn(self.msg3.id, ids)
        msg = messages[0]
        self.assertIn('id', msg)
        self.assertIn('text', msg)
        self.assertIn('sender', msg)
        self.assertIn('date_send', msg)

    def test_filters(self):
        self.client.force_authenticate(user=self.user1)
        response = self.client.get(self.url, data={'last_message': self.msg1.id})
        self.assertEqual(response.status_code, 200)
        data = response.json()
        ids = [msg['id'] for msg in data['results']]
        self.assertNotIn(self.msg1.id, ids)
        self.assertIn(self.msg2.id, ids)
        self.assertIn(self.msg3.id, ids)
        response = self.client.get(self.url, data={'limit': 1})
        self.assertEqual(response.status_code, 200)
        data = response.json()
        ids = [msg['id'] for msg in data['results']]
        self.assertNotIn(self.msg1.id, ids)
        self.assertNotIn(self.msg2.id, ids)
        self.assertIn(self.msg3.id, ids)

    def test_post(self):
        self.client.force_authenticate(user=self.user1)
        count = Message.objects.count()
        response = self.client.post(self.url, data={'text': 'Hello\nchat!<br>'})
        self.assertEqual(response.status_code, 201)
        self.assertEqual(Message.objects.count(), count + 1)
        msg = Message.objects.latest('id')
        self.assertEqual(msg.text, 'Hello<br>chat!&lt;br&gt;')


class LoginAPIViewTest(APITestCase):

    @classmethod
    def setUpTestData(cls):
        cls.user1 = UserFactory.create(
            username='foobar',
            email='foo@bar.com',
            password='123',
            first_name='Bar',
            last_name='Foo'
        )
        cls.url = reverse('api-login')

    def test_post(self):
        incorrect_data = {
            'username': 'foo',
            'password': '1'
        }
        response = self.client.post(self.url, data=incorrect_data)
        self.assertEqual(response.status_code, 403)
        data = response.json()
        self.assertIn('__form__', data)
