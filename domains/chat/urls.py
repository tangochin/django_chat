from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.auth.views import LogoutView
from django.urls import include
from rest_framework.routers import DefaultRouter

from .api import MessagesViewSet, login_api_view
from .views import LoginView, ChatView

api_router = DefaultRouter()
api_router.register('messages', MessagesViewSet, 'api-messages')


urlpatterns = [
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(r'^logout/$', LogoutView.as_view(), name='logout'),
    url(r'^$', ChatView.as_view(), name='chat'),

    url(r'^api/v1/', include(api_router.urls)),
    url(r'^api/v1/login/$', login_api_view, name='api-login'),

    url(r'^admin/', admin.site.urls),
] + static(
    settings.MEDIA_URL,
    document_root=settings.MEDIA_ROOT)
