from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views.generic import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin


class LoginView(TemplateView):
    template_name = 'login.html'

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return HttpResponseRedirect(reverse('chat'))
        return super().dispatch(request, *args, **kwargs)


class ChatView(LoginRequiredMixin, TemplateView):
    template_name = 'chat.html'
