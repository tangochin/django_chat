import Cookies from 'js-cookie';


export function getFromApi(url, data) {
    return $.ajax({
        url: url,
        contentType: 'application/json; charset=UTF-8',
        data: data,
        headers: {
            'X-CSRFToken': Cookies.get('csrftoken')
        },
        dataType: 'json',
        method: 'GET'
    });
}

export function postToApi(url, data) {
    return $.ajax({
        url: url,
        contentType: 'application/json; charset=UTF-8',
        data: JSON.stringify(data),
        headers: {
            'X-CSRFToken': Cookies.get('csrftoken')
        },
        dataType: 'json',
        method: 'POST'
    });
}
