import React from 'react';


export default class ChatMessage extends React.Component {

    render() {
        return (
            <div className="message">
              <div className="user">{this.props.message.sender}:</div>
              <div className="content" dangerouslySetInnerHTML={{__html: this.props.message.text}}></div>
            </div>
        )
    }
}
