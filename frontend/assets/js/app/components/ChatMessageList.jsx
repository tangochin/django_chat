import React from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';

import ChatMessage from './ChatMessage';


class ChatMessageList extends React.Component {

    componentDidUpdate() {
        // Scroll down after update message list
        const chat = ReactDOM.findDOMNode(this).parentNode;
        chat.scrollTop = chat.scrollHeight;
    }

    render() {
        const {messages} = this.props;
        const chatMessages = messages.map((message) =>
            <ChatMessage key={message.id} message={message} />
        );

        return (
            <div>
                {chatMessages}
            </div>
        )
    }
}


export function mapStateToProps(state) {
    return {
        messages: state.chat.messages
    }
}


export default connect(mapStateToProps)(ChatMessageList)
