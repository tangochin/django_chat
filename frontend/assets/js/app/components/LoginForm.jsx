import React from 'react';

import { Formik } from 'formik';

import { postToApi } from '../common/api';


const LOGIN_API_URL = '/api/v1/login/';


function login(username, password) {
    let data = {
        'username': username,
        'password': password
    };
    return postToApi(LOGIN_API_URL, data)
}


export default class LoginForm extends React.Component {

    render() {
        return (
            <div>
                <Formik
                    initialValues={{username: '', password: ''}}
                    validate={values => {
                        let errors = {};
                        if (!values.username) {
                            errors.username = 'Username is required.';
                        }
                        if (!values.password) {
                            errors.password = 'Password is required.';
                        }
                        return errors;
                    }}
                    onSubmit={(values, actions) => {
                        actions.setSubmitting(false);
                        login(values.username, values.password)
                            .done(() => {
                                window.location = '/';
                            })
                            .fail((data) => {
                                actions.setErrors(data.responseJSON);
                            })
                    }}
                >
                    {({
                          values,
                          errors,
                          touched,
                          handleChange,
                          handleBlur,
                          handleSubmit,
                          isSubmitting,
                      }) => {
                        const groupClass = 'form-group';
                        const errorGroupClass = groupClass + ' has-error';

                        let formError;
                        if (errors.__form__) {
                            formError = (
                                <div className="row">
                                    <div className="col-md-offset-3 col-md-6">
                                        <div className="alert alert-danger alert-dismissible fade in" role="alert">
                                            <button type="button" className="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">×</span></button>{errors.__form__}
                                        </div>
                                    </div>
                                </div>
                            )
                        }

                        return (
                            <form onSubmit={handleSubmit} className="login-form">
                                {formError}
                                <div className="row">
                                    <div className="col-md-offset-3 col-md-6">
                                        <div className={!!errors.username ? errorGroupClass : groupClass}>
                                            <label className="control-label" htmlFor="username">Username</label>
                                            <input
                                                type="text"
                                                name="username"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.username}
                                                placeholder="Username"
                                                className="form-control"
                                            />
                                            <span className="help-block">
                                            {errors.username && touched.username && errors.username}
                                        </span>
                                        </div>
                                        <div className={!!errors.password ? errorGroupClass : groupClass}>
                                            <label className="control-label" htmlFor="password">Password</label>
                                            <input
                                                type="password"
                                                name="password"
                                                onChange={handleChange}
                                                onBlur={handleBlur}
                                                value={values.password}
                                                placeholder="Password"
                                                className="form-control"
                                            />
                                            <span className="help-block">
                                            {errors.password && touched.password && errors.password}
                                        </span>
                                        </div>
                                        <button type="submit" disabled={isSubmitting} className="btn btn-default">
                                            Log in
                                        </button>
                                    </div>
                                </div>
                            </form>
                        )
                    }}
                </Formik>
            </div>
        )
    }
}
