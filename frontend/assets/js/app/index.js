import { initChat } from './modules/chat';
import { initLoginForm } from './modules/login';

require('bootstrap-webpack');


$(function () {
    initChat();
    initLoginForm();
});
