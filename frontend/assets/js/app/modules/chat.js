import React from 'react';
import ReactDOM from 'react-dom';
import { combineReducers, createStore } from 'redux';
import { getFromApi, postToApi } from '../common/api';
import ChatMessageList from '../components/ChatMessageList';

const MESSAGES_API_URL = '/api/v1/messages/';
const MESSAGES_LIMIT = 1000;
const UPDATE_INTERVAL = 1000;

const LOAD_ALL_CHAT_MESSAGES = 'LOAD_ALL_CHAT_MESSAGES';
const LOAD_NEW_CHAT_MESSAGES = 'LOAD_NEW_CHAT_MESSAGES';


export const initialChatState = {
    chat: {
        messages: []
    }
};


export const initialState = {
    chat: initialChatState.chat
};


export const reducer = combineReducers({
    chat: chatReducer
});


export const store = createStore(reducer, initialState);


export function chatReducer(state = initialChatState, action) {
    switch (action.type) {
        case LOAD_ALL_CHAT_MESSAGES: {
            return Object.assign({}, state, {
                messages: action.messages
            });
        }
        case LOAD_NEW_CHAT_MESSAGES: {
            return Object.assign({}, state, {
                messages: [...state.messages, ...action.messages]
            });
        }
        default:
            return state
    }
}


export function loadChatMessagesCreator(messages) {
    return {
        type: LOAD_ALL_CHAT_MESSAGES,
        messages: messages
    }
}


export function loadNewMessagesCreator(messages) {
    return {
        type: LOAD_NEW_CHAT_MESSAGES,
        messages: messages
    }
}


function loadChatMessages(lastMessageId=null, limit=null) {
    let data = {};
    if (lastMessageId) {
        data['last_message'] = lastMessageId;
    }
    if (limit) {
        data['limit'] = limit;
    }
    return getFromApi(MESSAGES_API_URL, data)
}


function sendChatMessage(text) {
    return postToApi(MESSAGES_API_URL, {'text': text})
}


function renderChatMessageList() {
    const el = document.getElementById('chat');
    if (el) {
        return ReactDOM.render(
            React.createElement(ChatMessageList, {store: store}),
            el
        );
    }
}


function initChatForm() {
    const textbox = $('#textbox');
    const submit = $('#submit');

    textbox.focus();

    function send() {
        sendChatMessage(textbox.val())
            .done((data) => {
                textbox.val('');
                textbox.focus();
            })
    }

    submit.click(send);

    textbox.keydown((e) => {
        if (e.ctrlKey && e.keyCode === 13) {
            send();
        }
    })
}


export function initChat() {
    const chatEl = document.getElementById('chat');
    let lastMessageId = null;

    renderChatMessageList();

    if (chatEl) {
        loadChatMessages(lastMessageId, MESSAGES_LIMIT).done(
            (data) => {
                if (data.results.length) {
                    store.dispatch(
                        loadChatMessagesCreator(data.results)
                    );
                    let lastMessage = data.results[data.results.length - 1];
                    lastMessageId = lastMessage.id
                }
            }
        );

        setInterval(() => {
            loadChatMessages(lastMessageId).done(
                (data) => {
                    if (data.results.length) {
                        store.dispatch(
                            loadNewMessagesCreator(data.results)
                        );
                        let lastMessage = data.results[data.results.length - 1];
                        lastMessageId = lastMessage.id
                    }
                }
            );
        }, UPDATE_INTERVAL);
    }

    initChatForm();
}
