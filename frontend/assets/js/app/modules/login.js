import ReactDOM from 'react-dom';
import React from 'react';

import LoginForm from '../components/LoginForm';


function renderLoginForm() {
    const el = document.getElementById('login');
    if (el) {
        return ReactDOM.render(
            React.createElement(LoginForm),
            el
        );
    }
}


export function initLoginForm() {
    renderLoginForm();
}
