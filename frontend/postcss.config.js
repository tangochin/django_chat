module.exports = {
    plugins: [
        require('precss')(),
        require('autoprefixer')({browsers: ['> 1%', 'IE 8']})
    ]
};
