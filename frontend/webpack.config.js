const path = require('path');
const merge = require('webpack-merge');
const webpack = require('webpack');
const extractTextPlugin = require('extract-text-webpack-plugin');


const TARGET = process.env.npm_lifecycle_event;
const CONFIG = {
    APP: path.join(__dirname, 'assets', 'js', 'app'),
    STYLES: path.join(__dirname, 'assets', 'css'),
    BUILD: path.join(__dirname, 'build'),
    FONTS_DIR: 'fonts/',
    IMG_DIR: 'img/',
    PUBLIC_PATH: '/static/build/',
};


let settings = {
    entry: {
        app: [
            './assets/js/app/index.js',
        ],
        styles: [
            'bootstrap-webpack!./bootstrap.config.js',
            './assets/css/styles.css'
        ],
        vendors: ['jquery', '!bootstrap-webpack/bootstrap-scripts.loader!./bootstrap.config.js']
    },
    output: {
        path: CONFIG.BUILD,
        filename: '[name].bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options: {
                    presets: ['react']
                }
            },
            {
                test: /\.css$/,
                loader: extractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [
                        {
                            loader: 'css-loader',
                            options: {
                                minimize: true
                            }
                        }
                    ]
                })
            },
            {
                test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)|\.svg($|\?)/,
                loader: 'url-loader'
            },
            {
                test: /\.html$/,
                loader: 'html-loader'
            }
        ]
    },
    node: {
        fs: 'empty'
    },
    plugins: [
        new extractTextPlugin('[name].css'),
        new webpack.ProvidePlugin({
           $: 'jquery',
           jQuery: 'jquery',
           'window.jQuery': 'jquery'
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: 'vendors',
            minChunks: Infinity,
            async: true
        })
    ],
    resolve: {
        extensions: ['.js', '.jsx']
    }
};

if (TARGET === 'build') {
    settings = merge(settings, {
        plugins: [
            new webpack.DefinePlugin({
                'process.env.NODE_ENV': JSON.stringify('production')
            }),
            new webpack.optimize.UglifyJsPlugin({
                'process.env.NODE_ENV': JSON.stringify('production'),
                sourceMap: true
            })
        ],
        devtool: 'source-map'
    });
} else {
    settings = merge(settings, {
        devtool: 'eval-source-map'
    });
}


module.exports = settings;
